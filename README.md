# Commons-data-mapping
A Library to manipulate Key-Value Maps in a Type Safe Manner.

## Maven Coordinates
### Repository
Emerjoin OSS repository
```xml
<repository>
    <id>emerjoin-oss</id>
    <name>release</name>
    <url>https://pkg.emerjoin.org/oss</url>
</repository>
```

### Artifact
Latest version
```xml
<dependency>
    <groupId>org.emerjoin.commons</groupId>
    <artifactId>commons-data-mapping</artifactId>
    <version>1.1.1-final</version>
</dependency>
```

## Features
* TypedDataMap - encapsulates a **Map<String,Object>** and exposes type-safe **put** and **get** methods:
    * get
    * getOrDefault
    * getOptional
* Data Proxy objects - define an interface with **get** and **set** methods that can be used to manipulate a **Map<String,Object>** via a **TypedDataMap**

## Usage example:

### TypedDataMap
```java
    Map<String,Object> valuesMap = new HashMap<>();
    TypedDataMap typedDataMap = new TypedDataMap(valuesMap);
    typedDataMap.putString("name","John Doe");
    typedDataMap.putInt("age",27);
    typedDataMap.putFloat("height",1,73);

    Optional<Double> optionalWeight = typedDataMap.getOptionalDouble("weight");
    boolean isAlive = typedDataMap.getBooleanOrDefault("alive",true);

```

### Data Proxy objects
Defining the proxy type
```java
    public interface Person extends DataProxyType {
        
        @Property("person.name")
        String getName();
        void setName(String name);
    
        @Property("person.age")
        int getAge();
        void setAge(int age);
        
        @Property("person.height")
        float getHeight();
        void setHeight(float height);
    
        @Property("person.weight")
        Optional<Double> getWeight();
        void setWeight(double weight);

    }
```
Creating an instance of the proxy type;
```java
    Map<String,Object> map = new HashMap<>();
    map.put("person.name","John Doe");
    map.put("person.age",35);
    map.put("person.height",1.75);

    TypedDataMap typedDataMap = new TypedDataMap(map);
    Person proxy = DataProxy.make(Person.class,typedDataMap);
    int age = proxy.getAge(); //Returns 35
    String name = proxy.getName(); //Returns John Doe
    float height = proxy.getHeight(); //Return 1.75;
    proxy.setWeight(85.15);
    Optional<Double> optionalWeight = proxy.getWeight();
    optionalWeight.isPresent(); //Returns true
    
```