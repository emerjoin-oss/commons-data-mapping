import org.emerjoin.commons.data.mapping.Converters;
import org.emerjoin.commons.data.mapping.TypedDataMap;
import org.emerjoin.commons.data.mapping.ValueConverterNotFoundException;
import org.emerjoin.commons.data.mapping.ValueWritePolicy;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;

public class TypedDataMapTest {

    private static final String KEY_NAME = "name";
    private static final String KEY_AGE = "age";
    private static final String KEY_HEIGHT = "height";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_CUSTOM_OBJ = "customObj";

    @Test
    public void get_optional_must_return_empty_optional(){

        Map<String,Object> objectMap = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(), ValueWritePolicy.
                CONVERT_TO_STRING);

        assertFalse(typedDataMap.getOptional(KEY_NAME,String.class).
                isPresent());

    }

    @Test
    public void get_optional_string_must_return_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_NAME,"Mario");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertTrue(typedDataMap.getOptional(KEY_NAME,String.class).
                isPresent());

    }

    @Test
    public void get_optional_int_must_return_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE,"27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertTrue(typedDataMap.getOptionalInt(KEY_AGE).
                isPresent());

    }

    @Test
    public void get_int_must_return_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE,"27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals( Integer.valueOf(27),typedDataMap.getInt(KEY_AGE));

    }

    @Test
    public void get_optional_long_must_return_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE,"27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertTrue(typedDataMap.getOptionalLong(KEY_AGE).
                isPresent());

    }

    @Test
    public void get_long_must_return_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE,"27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals( Long.valueOf(27),typedDataMap.getLong(
                KEY_AGE));

    }

    @Test
    public void get_optional_byte_must_return_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE,"27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertTrue(typedDataMap.getOptionalByte(KEY_AGE).
                isPresent());

    }

    @Test
    public void get_byte_must_return_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE, (byte) 27);
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals( Byte.valueOf((byte) 27),typedDataMap.getByte(
                KEY_AGE));

    }

    @Test
    public void get_optional_short_must_return_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE, (short) 27);
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertTrue(typedDataMap.getOptionalShort(KEY_AGE).
                isPresent());

    }

    @Test
    public void get_short_must_return_short_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE,(short) 27);
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Short.valueOf((short) 27),typedDataMap.getShort(
                KEY_AGE));

    }

    @Test
    public void get_optional_float_must_return_float_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_HEIGHT,1.37f);
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertTrue(typedDataMap.getOptionalFloat(KEY_HEIGHT).
                isPresent());

    }

    @Test
    public void get_float_must_return_float_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_HEIGHT,1.73f);
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Float.valueOf(1.73f),typedDataMap.getFloat(
                KEY_HEIGHT));

    }

    @Test
    public void get_optional_float_must_automatically_convert_double_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_HEIGHT,1.73);
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        Optional<Float> optionalFloat = typedDataMap.getOptionalFloat(KEY_HEIGHT);
        assertTrue(optionalFloat.isPresent());
        assertEquals( Float.valueOf(1.73f),optionalFloat.get());

    }

    @Test
    public void get_float_must_automatically_convert_double_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_HEIGHT,1.73);
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Float.valueOf(1.73f),typedDataMap.getFloat(
                KEY_HEIGHT));

    }

    @Test
    public void get_optional_double_must_automatically_convert_float_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_HEIGHT,1.73f);
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        Optional<Double> optionalDouble = typedDataMap.getOptionalDouble(KEY_HEIGHT);
        assertTrue(optionalDouble.isPresent());

    }

    @Test
    public void get_double_must_automatically_convert_float_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_HEIGHT,1.73f);
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Double.valueOf(1.73f),typedDataMap.getDouble(
                KEY_HEIGHT));

    }

    @Test
    public void get_optional_boolean_must_return_true_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_SUCCESS,"true");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        Optional<Boolean> optionalBoolean = typedDataMap.getOptionalBoolean(KEY_SUCCESS);
        assertTrue(optionalBoolean.isPresent());
        assertTrue(optionalBoolean.get());

    }

    @Test
    public void get_optional_boolean_must_return_false_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_SUCCESS,"false");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        Optional<Boolean> optionalBoolean = typedDataMap.getOptionalBoolean(KEY_SUCCESS);
        assertTrue(optionalBoolean.isPresent());
        assertFalse(optionalBoolean.get());

    }

    @Test
    public void get_boolean_must_return_true_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_SUCCESS,"true");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertTrue(typedDataMap.getBoolean(KEY_SUCCESS));

    }

    @Test
    public void get_boolean_must_return_false_value_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_SUCCESS,"false");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertFalse(typedDataMap.getBoolean(KEY_SUCCESS));

    }


    @Test(expected = ValueConverterNotFoundException.class)
    public void attempt_to_get_CustomObj_must_fail_due_to_missing_converter(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_CUSTOM_OBJ, "EMPTY");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        typedDataMap.get(KEY_CUSTOM_OBJ,CustomObj.class);

    }

    @Test
    public void get_CustomObj_must_return_object_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_CUSTOM_OBJ,new CustomObj());
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertNotNull(typedDataMap.get(KEY_CUSTOM_OBJ,CustomObj.class));

    }

    @Test
    public void get_optional_CustomObj_must_return_object_from_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_CUSTOM_OBJ,new CustomObj());
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        Optional<CustomObj> optionalCustomObj = typedDataMap.getOptional(KEY_CUSTOM_OBJ,CustomObj.class);
        assertTrue(optionalCustomObj.isPresent());

    }


    @Test
    public void get_int_or_default_must_return_default_value_when_value_is_not_present(){

        Map<String,Object> objectMap = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Integer.valueOf(11),typedDataMap.getIntOrDefault(KEY_AGE,11));

    }

    @Test
    public void get_byte_or_default_must_return_default_value_when_value_is_not_present(){

        Map<String,Object> objectMap = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Byte.valueOf((byte) 11),typedDataMap.getByteOrDefault(KEY_AGE,(byte)11));

    }

    @Test
    public void get_short_or_default_must_return_default_value_when_value_is_not_present(){

        Map<String,Object> objectMap = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Short.valueOf((short)11),typedDataMap.getShortOrDefault(KEY_AGE,(short) 11));

    }

    @Test
    public void get_long_or_default_must_return_default_value_when_value_is_not_present(){

        Map<String,Object> objectMap = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Long.valueOf((short)11),typedDataMap.getLongOrDefault(KEY_AGE,11L));

    }

    @Test
    public void get_float_or_default_must_return_default_value_when_value_is_not_present(){

        Map<String,Object> objectMap = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Float.valueOf(11),typedDataMap.getFloatOrDefault(KEY_AGE,11f));

    }

    @Test
    public void get_double_or_default_must_return_default_value_when_value_is_not_present(){

        Map<String,Object> objectMap = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Double.valueOf(11.0),typedDataMap.getDoubleOrDefault(KEY_AGE,11.0));

    }


    @Test
    public void get_boolean_or_default_must_return_default_value_when_value_is_not_present(){

        Map<String,Object> objectMap = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Boolean.TRUE,typedDataMap.getBooleanOrDefault(KEY_AGE,true));
        assertEquals(Boolean.FALSE,typedDataMap.getBooleanOrDefault(KEY_AGE,false));

    }

    @Test
    public void put_int_must_cause_a_string_to_be_put_into_the_map_when_value_write_policy_is_CONVERT_TO_STRING(){

        Map<String,Object> mock = Mockito.mock(Map.class);
        TypedDataMap typedDataMap = new TypedDataMap(mock, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        typedDataMap.putInt(KEY_AGE,11);
        Mockito.verify(mock).put(KEY_AGE,"11");

    }

    @Test
    public void put_byte_must_cause_a_string_to_be_put_into_the_map_when_value_write_policy_is_CONVERT_TO_STRING(){

        Map<String,Object> mock = Mockito.mock(Map.class);
        TypedDataMap typedDataMap = new TypedDataMap(mock, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        typedDataMap.putByte(KEY_AGE,(byte) 11);
        Mockito.verify(mock).put(KEY_AGE,"11");

    }

    @Test
    public void put_short_must_cause_a_string_to_be_put_into_the_map_when_value_write_policy_is_CONVERT_TO_STRING(){

        Map<String,Object> mock = Mockito.mock(Map.class);
        TypedDataMap typedDataMap = new TypedDataMap(mock, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        typedDataMap.putShort(KEY_AGE, (short) 11);
        Mockito.verify(mock).put(KEY_AGE,"11");

    }

    @Test
    public void put_long_must_cause_a_string_to_be_put_into_the_map_when_value_write_policy_is_CONVERT_TO_STRING(){

        Map<String,Object> mock = Mockito.mock(Map.class);
        TypedDataMap typedDataMap = new TypedDataMap(mock, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        typedDataMap.putLong(KEY_AGE, 11L);
        Mockito.verify(mock).put(KEY_AGE,"11");

    }

    @Test
    public void put_float_must_cause_a_string_to_be_put_into_the_map_when_value_write_policy_is_CONVERT_TO_STRING(){

        Map<String,Object> mock = Mockito.mock(Map.class);
        TypedDataMap typedDataMap = new TypedDataMap(mock, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        typedDataMap.putFloat(KEY_HEIGHT, 3.14f);
        Mockito.verify(mock).put(KEY_HEIGHT,"3.14");

    }

    @Test
    public void put_double_must_cause_a_string_to_be_put_into_the_map_when_value_write_policy_is_CONVERT_TO_STRING(){

        Map<String,Object> mock = Mockito.mock(Map.class);
        TypedDataMap typedDataMap = new TypedDataMap(mock, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        typedDataMap.putDouble(KEY_HEIGHT, 3.14);
        Mockito.verify(mock).put(KEY_HEIGHT,"3.14");

    }

    @Test
    public void put_boolean_must_cause_a_string_to_be_put_into_the_map_when_value_write_policy_is_CONVERT_TO_STRING(){

        Map<String,Object> mock = Mockito.mock(Map.class);
        TypedDataMap typedDataMap = new TypedDataMap(mock, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        typedDataMap.putBoolean(KEY_SUCCESS, true);
        Mockito.verify(mock).put(KEY_SUCCESS,"true");
        typedDataMap.putBoolean(KEY_SUCCESS, false);
        Mockito.verify(mock).put(KEY_SUCCESS,"false");

    }

    @Test
    public void get_byte_must_return_value_converted_from_string_from_the_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE, "27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Byte.valueOf((byte) 27),typedDataMap.getByte(
                KEY_AGE));

    }

    @Test
    public void get_short_must_return_value_converted_from_string_from_the_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE, "27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Short.valueOf((short) 27),typedDataMap.getShort(
                KEY_AGE));

    }

    @Test
    public void get_int_must_return_value_converted_from_string_from_the_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE, "27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Integer.valueOf(27),typedDataMap.getInt(
                KEY_AGE));

    }

    @Test
    public void get_long_must_return_value_converted_from_string_from_the_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_AGE, "27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Long.valueOf(27),typedDataMap.getLong(
                KEY_AGE));
    }

    @Test
    public void get_float_must_return_value_converted_from_string_from_the_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_HEIGHT, "27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Float.valueOf(27),typedDataMap.getFloat(
                KEY_HEIGHT));
    }

    @Test
    public void get_double_must_return_value_converted_from_string_from_the_map(){

        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(KEY_HEIGHT, "27");
        TypedDataMap typedDataMap = new TypedDataMap(objectMap, Converters.defaults(),
                ValueWritePolicy.CONVERT_TO_STRING);
        assertEquals(Double.valueOf(27),typedDataMap.getDouble(
                KEY_HEIGHT));
    }





}
