import org.emerjoin.commons.data.mapping.proxing.DataProxyType;
import org.emerjoin.commons.data.mapping.proxing.DefaultValue;
import org.emerjoin.commons.data.mapping.proxing.Property;

import java.util.Optional;

public interface ExampleProxyType extends DataProxyType {

    @Property("age.int")
    Optional<Integer> getOptionalIntAge();

    int getIntAge();

    @Property("age.int")
    void setIntAge(int age);

    @Property("age.short")
    void setShortAge(short age);

    short getShortAge();

    @Property("age.short")
    Optional<Short> getOptionalShortAge();

    @Property("age.long")
    void setLongAge(long age);

    short getLongAge();

    @Property("age.byte")
    void setByteAge(byte age);

    byte getByteAge();

    @Property("height.float")
    void setFloatHeight(float age);

    float getFloatHeight();

    @Property("height.double")
    void setDoubleHeight(double age);

    double getDoubleHeight();

    @Property("name")
    void setName(String name);
    String getName();

    @Property("name")
    Optional<String> getOptionalName();

    @DefaultValue("http://google.co.mz")
    @Property("website")
    String getWebsite();


}
