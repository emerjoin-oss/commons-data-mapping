import org.emerjoin.commons.data.mapping.TypedDataMap;
import org.emerjoin.commons.data.mapping.proxing.DataProxy;
import org.emerjoin.commons.data.mapping.proxing.ValueNotFoundException;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;

public class DataProxyTest {

    @Test
    public void get_short_age_must_return_value_previously_set_using_setter_method(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        proxy.setShortAge((short) 101);
        short shortAge = proxy.getShortAge();
        assertEquals(101,shortAge);

    }

    @Test
    public void get_int_age_must_return_value_previously_set_using_setter_method(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        proxy.setIntAge(101);
        int intAge = proxy.getIntAge();
        assertEquals(101,intAge);

    }

    @Test
    public void get_long_age_must_return_value_previously_set_using_setter_method(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        proxy.setLongAge(105L);
        long longAge = proxy.getLongAge();
        assertEquals(105L,longAge);

    }

    @Test
    public void get_byte_age_must_return_value_previously_set_using_setter_method(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        proxy.setByteAge((byte) 27);
        byte byteAge = proxy.getByteAge();
        assertEquals(27,byteAge);

    }

    @Test
    public void get_float_height_must_return_value_previously_set_using_setter_method(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        proxy.setFloatHeight(1.73f);
        float floatHeight = proxy.getFloatHeight();
        assertEquals(1.73f, floatHeight,10);

    }

    @Test
    public void get_double_height_must_return_value_previously_set_using_setter_method(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        proxy.setDoubleHeight(1.73);
        double doubleHeight = proxy.getDoubleHeight();
        assertEquals(1.73f, doubleHeight,10);

    }

    @Test
    public void get_name_must_return_value_previously_set_using_setter_method(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        proxy.setName("John");
        String name = proxy.getName();
        assertEquals("John",name);

    }

    @Test
    public void get_optional_name_must_return_value_previously_set_using_setter_method(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        proxy.setName("John");
        Optional<String> optionalName = proxy.getOptionalName();
        assertTrue(optionalName.isPresent());
        assertEquals("John",optionalName.get());

    }

    @Test
    public void get_optional_name_must_return_empty_optional(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        Optional<String> optionalName = proxy.getOptionalName();
        assertFalse(optionalName.isPresent());

    }

    @Test(expected = ValueNotFoundException.class)
    public void get_name_must_cause_value_not_found_when_value_is_not_present(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        proxy.getName();

    }


    @Test
    public void get_website_must_return_default_value(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        assertEquals("http://google.co.mz",proxy.getWebsite());

    }

    @Test
    public void to_string_must_return_typed_data_map_to_string(){

        Map<String,Object> map = new HashMap<>();
        TypedDataMap typedDataMap = new TypedDataMap(map);
        typedDataMap.put("name","John", String.class);
        ExampleProxyType proxy = DataProxy.make(ExampleProxyType.class,typedDataMap);
        assertEquals("{name=John}",proxy.toString());

    }




}
