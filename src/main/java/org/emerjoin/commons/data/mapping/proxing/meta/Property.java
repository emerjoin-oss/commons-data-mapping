package org.emerjoin.commons.data.mapping.proxing.meta;


import org.emerjoin.commons.data.mapping.TypedDataMap;

public class Property {

    private String name;
    private String key;
    private SetterMethod setter;
    private GetterMethod getter;
    private String defaultValue;

    public enum MethodType {
        Get, Set
    }

    Property(String name, PropertyMethod method, MethodType methodType){
        this.name = name;
        if(methodType== MethodType.Get)
            this.getter = (GetterMethod) method;
        else this.setter = (SetterMethod) method;
        this.key = method.getKey();
    }

    public void setDefaultValue(String defaultValue){
        if(defaultValue==null)
            throw new IllegalArgumentException("defaultValue must not be null");
        this.defaultValue = defaultValue;
    }

    public void setter(SetterMethod method){
        if(method==null)
            throw new IllegalArgumentException("method must not be null");
        this.setter = method;
    }

    public void getter(GetterMethod method){
        if(method==null)
            throw new IllegalArgumentException("method must not be null");
        this.getter = method;
    }

    public void set(Object value, TypedDataMap typedDataMap){
        if(this.setter==null)
            throw new IllegalStateException("there is no setter method set for the property");
        this.setter.setValue(value, typedDataMap);
    }

    public Object get(TypedDataMap typedDataMap){
        if(this.getter==null)
            throw new IllegalStateException("there is no getter method set for the property");
        return this.getter.readValue(typedDataMap,
                defaultValue);
    }

    public String getKey() {
        return key;
    }

    public String getName(){
        return this.name;
    }

}
