package org.emerjoin.commons.data.mapping;

import org.emerjoin.commons.data.mapping.proxing.TypePair;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class TypedDataMap {

    private Map<String,Object> map;
    private ValueWritePolicy valueWrite;
    private Converters converters;

    public TypedDataMap(){
        this(new HashMap<>());
    }

    public TypedDataMap(Map<String,Object> map){
        this(map,Converters.defaults(), DataMappingConfigs.getInstance().getValueWritePolicy());
    }

    public TypedDataMap(Map<String,Object> map, Converters converters){
        this(map,converters, ValueWritePolicy.RAW_VALUE);
    }

    public TypedDataMap(Map<String,Object> map, Converters converters, ValueWritePolicy valueWrite){
        if(map==null)
            throw new IllegalArgumentException("map must not be null");
        if(converters==null)
            throw new IllegalArgumentException("converters must not be null");
        if(valueWrite==null)
            throw new IllegalArgumentException("valueWrite must not be null");
        this.map = map;
        this.converters = converters;
        this.valueWrite = valueWrite;
    }


    private void checkKey(String key){
        if(key==null||key.isEmpty())
            throw new IllegalArgumentException("key must not be null nor empty");
    }

    private <A,B> ValueConverter<B> getConverter(Class<A> from, Class<B> type){
        Optional<ValueConverter<B>> optionalValueConverter = this.converters.get(from,
                type);
        if(!optionalValueConverter.isPresent())
            throw new ValueConverterNotFoundException(new TypePair(from,
                    type));
        return optionalValueConverter.get();
    }

    private <T> T getValue(String key, Class<T> type){
        this.checkKey(key);
        Object value = this.map.get(key);
        if(value==null)
            return null;
        if(type.isInstance(value))
            return (T) value;
        Class<?> fromType = value.getClass();
        if(fromType==type)
            return (T) value;
        ValueConverter<T> converter = this.getConverter(fromType,type);
        return converter.convert(value);
    }

    private <T> T getValueOrDefault(String key, Class<T> type, T def){
        T value = this.getValue(key,type);
        if(value==null)
            return def;
        return value;
    }

    private <T> Optional<T> getOptionalValue(String key, Class<T> type){
        return Optional.ofNullable(this.getValue(key,
                type));
    }

    public String getString(String key){
        this.checkKey(key);
        return this.getValue(key,String.class);
    }

    public String getStringOrDefault(String key, String def){
        this.checkKey(key);
        if(def==null)
            throw new IllegalArgumentException("def must not be null");
        return this.getValueOrDefault(key,String.class,def);
    }

    public Boolean getBoolean(String key){
        this.checkKey(key);
        return this.getValue(key,Boolean.class);
    }

    public Boolean getBooleanOrDefault(String key, boolean def){
        this.checkKey(key);
        return this.getValueOrDefault(key,Boolean.class,
                def);
    }

    public Float getFloat(String key){
        this.checkKey(key);
        return this.getValue(key,Float.class);
    }

    public Float getFloatOrDefault(String key, float def){
        this.checkKey(key);
        return this.getValueOrDefault(key,Float.class,
                def);
    }

    public Double getDouble(String key){
        this.checkKey(key);
        return this.getValue(key,Double.class);
    }

    public Double getDoubleOrDefault(String key, Double def){
        this.checkKey(key);
        return this.getValueOrDefault(key,Double.class,
                def);
    }

    public Integer getInt(String key){
        this.checkKey(key);
        return this.getValue(key, Integer.class);
    }

    public Integer getIntOrDefault(String key, Integer def){
        this.checkKey(key);
        return this.getValueOrDefault(key, Integer.class,
                def);
    }


    public Byte getByte(String key){
        this.checkKey(key);
        return this.getValue(key, Byte.class);
    }

    public Byte getByteOrDefault(String key, Byte def){
        this.checkKey(key);
        return this.getValueOrDefault(key, Byte.class,
                def);
    }

    public Long getLong(String key){
        this.checkKey(key);
        return this.getValue(key, Long.class);
    }


    public Long getLongOrDefault(String key, Long def){
        this.checkKey(key);
        return this.getValueOrDefault(key,Long.class,
                def);
    }

    public Short getShort(String key){
        this.checkKey(key);
        return this.getValue(key, Short.class);
    }

    public Short getShortOrDefault(String key, short def){
        this.checkKey(key);
        return this.getValueOrDefault(key,Short.class,
                def);
    }


    public <T> T get(String key, Class<T> type){
        this.checkKey(key);
        return this.getValue(key,type);
    }

    public Optional<String> getOptionalString(String key){
        this.checkKey(key);
        return this.getOptionalValue(key,String.class);
    }


    public Optional<Integer> getOptionalInt(String key){
        this.checkKey(key);
        return this.getOptionalValue(key,Integer.class);
    }

    public Optional<Short> getOptionalShort(String key){
        this.checkKey(key);
        return this.getOptionalValue(key,Short.class);
    }

    public Optional<Float> getOptionalFloat(String key){
        this.checkKey(key);
        return this.getOptionalValue(key,Float.class);
    }

    public Optional<Double> getOptionalDouble(String key){
        this.checkKey(key);
        return this.getOptionalValue(key,Double.class);
    }

    public Optional<Long> getOptionalLong(String key){
        this.checkKey(key);
        return this.getOptionalValue(key,Long.class);
    }

    public Optional<Byte> getOptionalByte(String key){
        this.checkKey(key);
        return this.getOptionalValue(key,Byte.class);
    }

    public Optional<Boolean> getOptionalBoolean(String key){
        this.checkKey(key);
        return this.getOptionalValue(key,Boolean.class);
    }


    public <T> Optional<T> getOptional(String key, Class<T> type){
        this.checkKey(key);
        return this.getOptionalValue(key,
                type);
    }

    public void put(String key, Object value, Class<?> type){
        this.checkKey(key);
        if(value==null)
            throw new IllegalArgumentException("value must not be null");
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        if(valueWrite== ValueWritePolicy.RAW_VALUE)
            this.map.put(key,value);
        if(value.getClass()==String.class) {
            this.map.put(key, value);
            return;
        }
        ValueConverter<String> converter = getConverter(type,String.class);
        this.map.put(key,converter.convert(value));
    }

    public <T> T getOrDefaultFromString(String key, Class<T> type, String def){
        this.checkKey(key);
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        if(def==null)
            throw new IllegalArgumentException("def must not be null");
        T value = getValue(key,type);
        if(value==null) {
            if(type==String.class)
                return (T) def;
            return this.getConverter(String.class, type).convert(def);
        }
        return value;
    }

    private void checkValue(Object value){
        if(value==null)
            throw new IllegalArgumentException("value must not be null");
    }

    public void putString(String key, String value){
        this.checkKey(key);
        this.checkValue(value);
        this.map.put(key,value);
    }


    public void putBoolean(String key, Boolean value){
        this.checkKey(key);
        this.checkValue(value);
        this.put(key,value,Boolean.class);
    }

    public void putByte(String key, Byte value){
        this.checkKey(key);
        this.checkValue(value);
        this.put(key,value,Byte.class);
    }

    public void putInt(String key, Integer value){
        this.checkKey(key);
        this.checkValue(value);
        this.put(key,value,Integer.class);
    }

    public void putShort(String key, Short value){
        this.checkKey(key);
        this.checkValue(value);
        this.put(key,value,Short.class);
    }

    public void putLong(String key, Long value){
        this.checkKey(key);
        this.checkValue(value);
        this.put(key,value,Long.class);
    }

    public void putFloat(String key, Float value){
        this.checkKey(key);
        this.checkValue(value);
        this.put(key,value,Float.class);
    }


    public void putDouble(String key, Double value){
        this.checkKey(key);
        this.checkValue(value);
        this.put(key,value,Double.class);
    }

    public void remove(String key){
        this.checkKey(key);
        this.map.remove(key);
    }

    public Map<String,Object> map(){
        return this.map;
    }

}
