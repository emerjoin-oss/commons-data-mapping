package org.emerjoin.commons.data.mapping.proxing;


import org.emerjoin.commons.data.mapping.TypedDataMap;
import org.emerjoin.commons.data.mapping.proxing.meta.DataProxyMetadata;
import org.emerjoin.commons.data.mapping.proxing.meta.MethodInfo;
import org.emerjoin.commons.data.mapping.proxing.meta.Property;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public final class DataProxy implements InvocationHandler {

    private static final Map<String, DataProxyMetadata> METADATA = new HashMap<>();
    private static final String TO_STRING_METHOD_NAME = "toString";

    private DataProxyMetadata metadata;
    private TypedDataMap typedDataMap;
    private DataProxy(DataProxyMetadata metadata, TypedDataMap typedDataMap){
        this.metadata = metadata;
        this.typedDataMap = typedDataMap;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if(method.getName().equals(TO_STRING_METHOD_NAME))
            return typedDataMap.map()
                    .toString();
        MethodInfo methodInfo = new MethodInfo(method);
        Property property = metadata.getPropertyByName(methodInfo.propertyName().get());
        if(methodInfo.type()==MethodInfo.MethodType.Getter)
            return property.get(typedDataMap);
        else property.set(args[0],typedDataMap);
        return null;
    }

    public static  <T extends DataProxyType> T make(Class<T> type, TypedDataMap typedDataMap){
        if(type==null||!type.isInterface())
            throw new IllegalArgumentException("type must not be null and must be an interface");
        if(typedDataMap==null)
            throw new IllegalArgumentException("typedDataMap must not be null");
        DataProxyMetadata metadata = METADATA.get(type.getCanonicalName());
        if(metadata==null){
            metadata = new DataProxyMetadata(type);
            METADATA.put(type.getCanonicalName(),metadata);
        }
        Class[] interfaces = new Class[]{type};
        return (T) Proxy.newProxyInstance(type.getClassLoader(),
                interfaces,new DataProxy(metadata, typedDataMap));
    }

}
