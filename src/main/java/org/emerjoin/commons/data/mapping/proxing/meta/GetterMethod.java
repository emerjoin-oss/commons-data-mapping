package org.emerjoin.commons.data.mapping.proxing.meta;


import org.emerjoin.commons.data.mapping.TypedDataMap;
import org.emerjoin.commons.data.mapping.proxing.ValueNotFoundException;

import java.lang.reflect.Method;
import java.util.Optional;

public class GetterMethod extends PropertyMethod {

    GetterMethod(Method method, String key) {
        super(method, key, DataType.Return);
    }

    public Object readValue(TypedDataMap typedDataMap, String defaultValue){
        if(typedDataMap==null)
            throw new IllegalArgumentException("typedDataMap must not be null");
        Object value = typedDataMap.get(getKey(),getValueType());
        if(value==null&&defaultValue!=null){
            value = typedDataMap.getOrDefaultFromString(getKey(),getValueType(),
                    defaultValue);
        }
        if(value==null&&!isOptional())
            throw new ValueNotFoundException(getKey());
        else if(value!=null&&isOptional()){
            return Optional.of(value);
        }else if(isOptional())
            return Optional.empty();
        return value;
    }

}
