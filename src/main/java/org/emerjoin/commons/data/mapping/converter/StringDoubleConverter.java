package org.emerjoin.commons.data.mapping.converter;


import org.emerjoin.commons.data.mapping.ConverterException;
import org.emerjoin.commons.data.mapping.ValueConverter;

public class StringDoubleConverter implements ValueConverter<Double> {

    @Override
    public Double convert(Object value) throws ConverterException {
        return Double.parseDouble(value.toString());
    }

}
