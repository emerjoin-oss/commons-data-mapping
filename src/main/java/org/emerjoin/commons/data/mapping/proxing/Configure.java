package org.emerjoin.commons.data.mapping.proxing;

import org.emerjoin.commons.data.mapping.ValueWritePolicy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Configure {
    ValueWritePolicy write() default ValueWritePolicy.RAW_VALUE;
}
