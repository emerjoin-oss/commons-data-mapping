package org.emerjoin.commons.data.mapping.converter;


import org.emerjoin.commons.data.mapping.ConverterException;
import org.emerjoin.commons.data.mapping.ValueConverter;

public class StringLongConverter implements ValueConverter<Long> {

    @Override
    public Long convert(Object value) throws ConverterException {
        return Long.parseLong(value.toString());
    }


}
