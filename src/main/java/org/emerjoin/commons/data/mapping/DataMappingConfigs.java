package org.emerjoin.commons.data.mapping;

public class DataMappingConfigs {

    private static DataMappingConfigs INSTANCE = new DataMappingConfigs();

    public static DataMappingConfigs getInstance(){
        return INSTANCE;
    }

    private ValueWritePolicy valueWritePolicy = ValueWritePolicy.CONVERT_TO_STRING;

    public void setValueWritePolicy(ValueWritePolicy valueWritePolicy){
        if(valueWritePolicy==null)
            throw new IllegalArgumentException("valueWritePolicy must not be null");
        this.valueWritePolicy = valueWritePolicy;
    }

    public ValueWritePolicy getValueWritePolicy() {
        return valueWritePolicy;
    }
}
