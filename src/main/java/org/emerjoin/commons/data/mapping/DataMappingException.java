package org.emerjoin.commons.data.mapping;

public class DataMappingException extends RuntimeException {

    public DataMappingException(String message){
        super(message);
    }

    public DataMappingException(String message, Throwable cause){
        super(message,cause);
    }

}
