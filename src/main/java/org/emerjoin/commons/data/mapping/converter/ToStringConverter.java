package org.emerjoin.commons.data.mapping.converter;


import org.emerjoin.commons.data.mapping.ConverterException;
import org.emerjoin.commons.data.mapping.ValueConverter;

public class ToStringConverter implements ValueConverter<String> {

    @Override
    public String convert(Object value) throws ConverterException {
        return value.toString();
    }

}
