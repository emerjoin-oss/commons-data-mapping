package org.emerjoin.commons.data.mapping.proxing.meta;



import java.lang.reflect.*;
import java.util.Optional;

public class PropertyMethod {

    private String key;
    private Method actual;
    private Class<?> valueType;
    private boolean optional = false;

    enum DataType {
        Return,Argument
    }

    PropertyMethod(Method method, String key, DataType dataType){
        if(dataType== DataType.Argument){
            Parameter[] parameters = method.getParameters();
            this.valueType = parameters[0].getType();
        }else{
            this.valueType = method.getReturnType();
            if (valueType== Optional.class){
                this.optional = true;
                this.valueType = (Class<?>)  ( (ParameterizedType) method.getGenericReturnType()).getActualTypeArguments()[0];
            }
        }
        this.key = key;
        this.actual = method;
    }


    protected boolean isOptional(){
        return this.optional;
    }

    protected String getKey(){
        return this.key;
    }

    public boolean match(Method method){
        return this.actual == method;
    }

    protected Class<?> getValueType(){
        return this.valueType;
    }

}
