package org.emerjoin.commons.data.mapping;

import org.emerjoin.commons.data.mapping.proxing.TypePair;

public class ValueConverterNotFoundException extends ValueConversionException {

    public ValueConverterNotFoundException(TypePair typePair) {
        super("There is no registered Converter for: "+typePair);
    }
}
