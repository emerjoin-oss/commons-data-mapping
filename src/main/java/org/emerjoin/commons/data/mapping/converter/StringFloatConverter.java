package org.emerjoin.commons.data.mapping.converter;


import org.emerjoin.commons.data.mapping.ConverterException;
import org.emerjoin.commons.data.mapping.ValueConverter;

public class StringFloatConverter implements ValueConverter<Float> {

    @Override
    public Float convert(Object value) throws ConverterException {
        return Float.parseFloat(value.toString());
    }

}
