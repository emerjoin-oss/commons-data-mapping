package org.emerjoin.commons.data.mapping.proxing;


public class ValueNotFoundException extends DataProxyException {

    public ValueNotFoundException(String key) {
        super(String.format("There is no value for key [%s] in the provided DataStore. Maybe you should use Optional return type",
                key));
    }
}
