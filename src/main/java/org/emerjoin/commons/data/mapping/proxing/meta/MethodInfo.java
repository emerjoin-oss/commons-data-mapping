package org.emerjoin.commons.data.mapping.proxing.meta;

import org.emerjoin.commons.data.mapping.proxing.DefaultValue;

import java.lang.reflect.Method;
import java.util.Optional;

public class MethodInfo {

    public enum MethodType {
        Getter, Setter, Unknown
    }

    private String propertyName;
    private Method method;
    private String defaultValue;
    private MethodType methodType = MethodType.Unknown;

    public MethodInfo(Method method){
        if(method==null)
            throw new IllegalArgumentException("method must not be null");
        this.method = method;
        String name = method.getName();
        if(name.startsWith("set"))
            this.methodType = MethodType.Setter;
        else if(name.startsWith("get")) {
            this.methodType = MethodType.Getter;
            DefaultValue defaultValue = method.getAnnotation(DefaultValue.class);
            if(defaultValue!=null)
                this.defaultValue = defaultValue.value();
        }
        if(methodType!= MethodType.Unknown)
            this.propertyName = name.substring(3);
    }

    public Optional<String> propertyName(){
        return Optional.ofNullable(
                propertyName);
    }

    public boolean hasDefaultValue(){
        return defaultValue!=null;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public MethodType type(){
        return this.methodType;
    }

    public Method method(){
        return this.method;
    }


}
