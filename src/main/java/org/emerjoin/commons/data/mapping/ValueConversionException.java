package org.emerjoin.commons.data.mapping;

public class ValueConversionException extends DataMappingException {

    public ValueConversionException(String message) {
        super(message);
    }

    public ValueConversionException(String message, Throwable cause) {
        super(message, cause);
    }
}
