package org.emerjoin.commons.data.mapping.proxing;


public class InvalidDataTypeException extends DataProxyException {

    public InvalidDataTypeException(String message) {
        super(message);
    }
}
