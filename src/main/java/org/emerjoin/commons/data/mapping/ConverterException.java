package org.emerjoin.commons.data.mapping;


public class ConverterException extends ValueConversionException {

    public ConverterException(String message) {
        super(message);
    }

    public ConverterException(String message, Throwable cause) {
        super(message, cause);
    }
}
