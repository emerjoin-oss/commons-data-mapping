package org.emerjoin.commons.data.mapping.converter;

import org.emerjoin.commons.data.mapping.ConverterException;
import org.emerjoin.commons.data.mapping.ValueConverter;

public class DoubleToFloatConverter implements ValueConverter<Float> {

    @Override
    public Float convert(Object value) throws ConverterException {
        return ((Double) value).floatValue();
    }

}
