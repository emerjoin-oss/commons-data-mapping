package org.emerjoin.commons.data.mapping.converter;


import org.emerjoin.commons.data.mapping.ConverterException;
import org.emerjoin.commons.data.mapping.ValueConverter;

public class StringBooleanConverter implements ValueConverter<Boolean> {

    @Override
    public Boolean convert(Object value) throws ConverterException {
        return Boolean.parseBoolean(value.toString());
    }

}
