package org.emerjoin.commons.data.mapping;

import org.emerjoin.commons.data.mapping.proxing.TypePair;
import org.emerjoin.commons.data.mapping.converter.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public final class Converters {

    private static Converters DISCOVERED = null;

    private Map<String,ValueConverter<?>> map = new HashMap<>();

    private Converters(){
        this.setDefaults();
    }

    private  <T> void putConverter(Class<?> from, Class<T> to, ValueConverter<T> converter){
        this.map.put(new TypePair(from,to).toString(),
                converter);
    }

    private void setDefaults(){
        ToStringConverter toStringConverter = new ToStringConverter();
        this.putConverter(Integer.class,String.class,toStringConverter);
        this.putConverter(Integer.TYPE,String.class,toStringConverter);
        this.putConverter(Short.class,String.class,toStringConverter);
        this.putConverter(Short.TYPE,String.class,toStringConverter);
        this.putConverter(Byte.class,String.class,toStringConverter);
        this.putConverter(Byte.TYPE,String.class,toStringConverter);
        this.putConverter(Boolean.class,String.class,toStringConverter);
        this.putConverter(Boolean.TYPE,String.class,toStringConverter);
        this.putConverter(Long.class,String.class,toStringConverter);
        this.putConverter(Long.TYPE,String.class,toStringConverter);
        this.putConverter(Float.class,String.class,toStringConverter);
        this.putConverter(Float.TYPE,String.class,toStringConverter);
        this.putConverter(Double.class,String.class,toStringConverter);
        this.putConverter(Double.TYPE,String.class,toStringConverter);

        this.putConverter(String.class,Integer.class,new StringIntegerConverter());
        this.putConverter(String.class,Integer.TYPE,new StringIntegerConverter());
        this.putConverter(String.class,Short.class,new StringShortConverter());
        this.putConverter(String.class,Short.TYPE,new StringShortConverter());
        this.putConverter(String.class,Long.class,new StringLongConverter());
        this.putConverter(String.class,Long.TYPE,new StringLongConverter());
        this.putConverter(String.class,Double.class,new StringDoubleConverter());
        this.putConverter(String.class,Double.TYPE,new StringDoubleConverter());
        this.putConverter(String.class,Float.class,new StringFloatConverter());
        this.putConverter(String.class,Float.TYPE,new StringFloatConverter());
        this.putConverter(String.class,Byte.class,new StringByteConverter());
        this.putConverter(String.class,Byte.TYPE,new StringByteConverter());
        this.putConverter(String.class,Boolean.class,new StringBooleanConverter());
        this.putConverter(String.class,Boolean.TYPE,new StringBooleanConverter());
        this.putConverter(Double.class,Float.class,new DoubleToFloatConverter());
        this.putConverter(Double.class,Float.TYPE,new DoubleToFloatConverter());
        this.putConverter(Float.class,Double.class,new FloatToDoubleConverter());
        this.putConverter(Float.class,Double.TYPE,new FloatToDoubleConverter());
    }

    public void register(TypePair typePair, ValueConverter<?> converter){
        if(typePair==null)
            throw new IllegalArgumentException("typPair must not be null");
        if(converter==null)
            throw new IllegalArgumentException("converter must not be null");
        this.map.put(typePair.toString(),converter);
    }

    public <A,B> Optional<ValueConverter<B>> get(Class<A> fromType, Class<B> toType){
        if(fromType==null)
            throw new IllegalArgumentException("fromType must not be null");
        if(toType==null)
            throw new IllegalArgumentException("toType must not be null");
        TypePair typePair = new TypePair(fromType,toType);
        ValueConverter<B> converter = (ValueConverter<B>) map.get(typePair.toString());
        return  Optional.ofNullable(
                converter);
    }

    public static Converters defaults(){

        return new Converters();

    }


}
