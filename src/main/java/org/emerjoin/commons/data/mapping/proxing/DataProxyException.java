package org.emerjoin.commons.data.mapping.proxing;

import org.emerjoin.commons.data.mapping.DataMappingException;

public class DataProxyException extends DataMappingException {

    public DataProxyException(String message){
        super(message);
    }

    public DataProxyException(String message, Throwable cause){
        super(message,cause);
    }

}
