package org.emerjoin.commons.data.mapping.proxing;

public class TypePair {

    private Class<?> fromType;
    private Class<?> toType;

    public TypePair(Class<?> fromType, Class<?> toType){
        this.fromType = fromType;
        this.toType = toType;
    }

    public Class<?> getFromType() {
        return fromType;
    }

    public Class<?> getToType() {
        return toType;
    }

    public String toString(){
        return String.format("TypePair[from=%s,to=%s]", fromType.getCanonicalName(), toType.
                getCanonicalName());
    }

}
