package org.emerjoin.commons.data.mapping.converter;

import org.emerjoin.commons.data.mapping.ConverterException;
import org.emerjoin.commons.data.mapping.ValueConverter;

public class StringShortConverter implements ValueConverter<Short> {

    @Override
    public Short convert(Object value) throws ConverterException {
        return Short.parseShort(value.toString());
    }

}
