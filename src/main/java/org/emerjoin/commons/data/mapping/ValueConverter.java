package org.emerjoin.commons.data.mapping;

public interface ValueConverter<ToType> {

    ToType convert(Object value) throws ConverterException;

}
