package org.emerjoin.commons.data.mapping.proxing.meta;



import org.emerjoin.commons.data.mapping.proxing.InvalidDataTypeException;

import java.lang.reflect.Method;

public class InvalidPropertyMethodException extends InvalidDataTypeException {

    public InvalidPropertyMethodException(Method method, String message) {
        super(String.format("Method %s:%s(..) %s",method.getDeclaringClass().getCanonicalName(),method.getName(),message));
    }
}
