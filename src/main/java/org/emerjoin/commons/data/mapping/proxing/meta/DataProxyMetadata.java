package org.emerjoin.commons.data.mapping.proxing.meta;


import org.emerjoin.commons.data.mapping.proxing.Property;
import org.emerjoin.commons.data.mapping.proxing.DataProxyType;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataProxyMetadata {

    private Class<? extends DataProxyType> type;
    private Map<String, org.emerjoin.commons.data.mapping.proxing.meta.Property> propertyMap = new HashMap<>();

    public DataProxyMetadata(Class<? extends DataProxyType> type){
        if(type==null)
            throw new IllegalArgumentException("type must not be null");
        this.type = type;
        this.discover();
    }

    public org.emerjoin.commons.data.mapping.proxing.meta.Property getPropertyByName(String name){

        return this.propertyMap.get(name);

    }

    private void discover(){
        Method[] methods = type.getMethods();
        List<Method> notAnnotatedMethods = new ArrayList<>();
        for(Method method: methods){
            Property persistenceKey = method.getAnnotation(Property.class);
            if(persistenceKey==null){
                notAnnotatedMethods.add(method);
                continue;
            }
            this.methodDiscovered(this.validateMethod(method));
        }
        for(Method method: notAnnotatedMethods){
            this.methodDiscovered(this.validateMethod(method));
        }
    }

    private String getPropertyKey(MethodInfo methodInfo){
        Method method = methodInfo.method();
        Property persistenceKey = method.getAnnotation(Property.class);
        if(persistenceKey==null){
            org.emerjoin.commons.data.mapping.proxing.meta.Property property = propertyMap.get(methodInfo.propertyName().get());
            if(property==null){
                throw new InvalidPropertyMethodException(method,
                        String.format("should either carry the @%s annotation or there should be a corresponding property method that carries the annotation",
                            Property.class.getSimpleName()));
            }
            return property.getKey();
        }
        return persistenceKey.value();
    }

    private void methodDiscovered(MethodInfo methodInfo){
        String propName = methodInfo.propertyName().get();
        org.emerjoin.commons.data.mapping.proxing.meta.Property property = propertyMap.get(propName);
        if(property==null){
            if(methodInfo.type()== MethodInfo.MethodType.Getter) {
                property = new org.emerjoin.commons.data.mapping.proxing.meta.Property(methodInfo.propertyName().get(), makeGetter(methodInfo),
                        org.emerjoin.commons.data.mapping.proxing.meta.Property.MethodType.Get);
                this.setDefaultValue(methodInfo,property);
                this.addProperty(property);
                return;
            }else{
                property = new org.emerjoin.commons.data.mapping.proxing.meta.Property(methodInfo.propertyName().get(), makeSetter(methodInfo),
                        org.emerjoin.commons.data.mapping.proxing.meta.Property.MethodType.Set);
                this.setDefaultValue(methodInfo,property);
                this.addProperty(property);
                return;
            }
        }else{
            if(methodInfo.type()== MethodInfo.MethodType.Setter)
                property.setter(makeSetter(methodInfo));
            else if(methodInfo.type() == MethodInfo.MethodType.Getter) {
                property.getter(makeGetter(methodInfo));
                this.setDefaultValue(methodInfo,property);
            }
        }
    }

    private void setDefaultValue(MethodInfo methodInfo, org.emerjoin.commons.data.mapping.proxing.meta.Property property){
        if(methodInfo.hasDefaultValue())
            property.setDefaultValue(methodInfo.getDefaultValue());
        this.addProperty(property);
    }

    private void addProperty(org.emerjoin.commons.data.mapping.proxing.meta.Property property){
        this.propertyMap.put(property.getName(),
                property);
    }

    private SetterMethod makeSetter(MethodInfo methodInfo){
        return new SetterMethod(methodInfo.method(),getPropertyKey(
                methodInfo));
    }

    private GetterMethod makeGetter(MethodInfo methodInfo){
        return new GetterMethod(methodInfo.method(),getPropertyKey(
                methodInfo));
    }

    private MethodInfo validateMethod(Method method){
        MethodInfo methodInfo = new MethodInfo(method);
        if(methodInfo.type()== MethodInfo.MethodType.Unknown)
            throw new InvalidPropertyMethodException(method,"not a getter nor a setter");
        this.validate(method,methodInfo.type());
        return methodInfo;
    }


    private void validate(Method method, MethodInfo.MethodType methodType){
        if(methodType== MethodInfo.MethodType.Getter)
            this.validateGetter(method);
        else this.validateSetter(method);
    }

    private void validateGetter(Method method){
        if(method.getReturnType()==Void.class) {
            throw new IllegalArgumentException(String.format("Method %s.%s() must not return Void",
                    method.getDeclaringClass().getCanonicalName(), method.
                            getName()));
        }
    }


    private void validateSetter(Method method){
        Parameter[] parameters = method.getParameters();
        if(parameters.length==0)
            throw new InvalidPropertyMethodException(method,
                    String.format("Method %s.%s() has zero parameters but should have exactly 1",
                    method.getDeclaringClass().getCanonicalName(),method.getName()));
        if(parameters.length>1)
            throw new InvalidPropertyMethodException(method,
                    String.format("Method %s.%s() has many parameters but should have exactly 1",
                    method.getDeclaringClass().getCanonicalName(),method.getName()));
    }


}
