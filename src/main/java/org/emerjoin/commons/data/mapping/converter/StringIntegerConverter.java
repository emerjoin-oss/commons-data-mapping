package org.emerjoin.commons.data.mapping.converter;

import org.emerjoin.commons.data.mapping.ConverterException;
import org.emerjoin.commons.data.mapping.ValueConverter;

public class StringIntegerConverter implements ValueConverter<Integer> {

    @Override
    public Integer convert(Object value) throws ConverterException {
        return Integer.parseInt(value.toString());
    }


}
