package org.emerjoin.commons.data.mapping.proxing.meta;


import org.emerjoin.commons.data.mapping.TypedDataMap;

import java.lang.reflect.Method;

public class SetterMethod extends PropertyMethod {

    SetterMethod(Method method, String key) {
        super(method, key, DataType.Argument);
    }

    public void setValue(Object value, TypedDataMap typedDataMap){
        if(value==null)
            typedDataMap.remove(getKey());
        else {
            typedDataMap.put(getKey(),value,getValueType());
        }
    }

}
