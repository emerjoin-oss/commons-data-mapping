package org.emerjoin.commons.data.mapping.converter;


import org.emerjoin.commons.data.mapping.ConverterException;
import org.emerjoin.commons.data.mapping.ValueConverter;

public class StringByteConverter implements ValueConverter<Byte> {

    @Override
    public Byte convert(Object value) throws ConverterException {
        return Byte.parseByte(value.toString());
    }

}
