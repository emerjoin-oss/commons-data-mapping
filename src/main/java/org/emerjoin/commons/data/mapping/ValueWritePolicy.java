package org.emerjoin.commons.data.mapping;

public enum ValueWritePolicy {
    RAW_VALUE, CONVERT_TO_STRING
}
